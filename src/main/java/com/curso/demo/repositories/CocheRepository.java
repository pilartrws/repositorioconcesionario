package com.curso.demo.repositories;

import com.curso.demo.model.Coche;
import com.curso.demo.model.EstadoCoche;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;


@Repository
public interface CocheRepository extends JpaRepository<Coche, Integer> {

    Optional<Coche> findByMatricula(String matricula);

    List<Coche> findByNumeroExposicion(int numeroExposicion);

    List<Coche> findByEstadoCoche(EstadoCoche estadoCoche);


}

package com.curso.demo.model;


import javax.persistence.*;

@Entity
public class Cliente {
    @Id@GeneratedValue(strategy= GenerationType.IDENTITY)
    @SequenceGenerator(name="ID", initialValue=1, allocationSize=100)
    private Integer id;
    private String name;
    private String direccion;
    private String dni;

    public Cliente() {

    }


    public Cliente (Integer id, String name, String direccion, String dni) {
        this.id = id;
        this.name = name;
        this.direccion = direccion;
        this.dni = dni;
    }



}

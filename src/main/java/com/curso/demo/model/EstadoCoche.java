package com.curso.demo.model;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;


public enum EstadoCoche {
    VENDIDO,
    REPARACION,
    ENSTOCK,
    RESERVADO,

}


package com.curso.demo.controllers;

import com.curso.demo.model.Coche;
import com.curso.demo.model.Reparacion;
import com.curso.demo.repositories.CocheEnReparacionRepository;
import com.curso.demo.repositories.CocheRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

//excepcion valores nulll
//metodos personalizados jpa
// exception handler

@RestController

public class ReparacionController {
    @Autowired
    private CocheRepository cocheRepository;
    @Autowired
    CocheEnReparacionRepository cocheEnReparacionRepository;

    //put,get
    @GetMapping("reparaciones/{cocheId}")//funciona
    public List buscarCoche(@PathVariable Integer cocheId) throws Exception {

        Optional<Coche> coches = cocheRepository.findById(cocheId);
        if (!coches.isPresent()) {
            throw new Exception("el coche con id" + cocheId + "no existe");
        }
        List<Reparacion> partes = cocheEnReparacionRepository.findByReparacionId(cocheId);
        if (partes.isEmpty()) {
            throw new Exception("No hay reparaciones que mostrar");
        }
        return partes;

    }


    @GetMapping("/reparaciones")
    public List coches() throws Exception {
        List<Reparacion> partes = cocheEnReparacionRepository.findAll();
        if (partes.isEmpty()) {
            throw new Exception("no hay partes de reparación");
        } else {
            return partes;
        }
    }


    @DeleteMapping("/reparaciones/{cocheId}")//funciona
    public void delecteCoche(@PathVariable Integer cocheId) throws Exception {
        Optional<Reparacion> partes = cocheEnReparacionRepository.findById(cocheId);
        if (partes.isPresent()) {
            cocheEnReparacionRepository.deleteById(cocheId);
        } else {
            throw new Exception("No hay partes registrados para este coche");
        }

    }


    @PutMapping("/updateParte")
    public Reparacion updateIt(@RequestBody Reparacion reparacion) throws Exception {


        Optional<Reparacion> partes = cocheEnReparacionRepository.findById(reparacion.getReparacionId());
        if (!partes.isPresent()) {
            throw new Exception("la exposición indicada no existe");
        }


        cocheEnReparacionRepository.save(reparacion);


        return reparacion;
    }


}






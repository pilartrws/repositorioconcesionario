package com.curso.demo.controllers;

import com.curso.demo.model.Coche;
import com.curso.demo.model.Exposicion;
import com.curso.demo.repositories.CocheRepository;
import com.curso.demo.repositories.ExposicionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@RestController
public class ExposicionController {
    @Autowired
    private ExposicionRepository exposicionRepository;
    @Autowired
    CocheRepository cocheRepository;


    @GetMapping("exposicion/{numeroExposicion}")
    public Exposicion buscarExpo(@PathVariable int numeroExposicion) throws Exception {

        Optional<Exposicion> exposicion = exposicionRepository.findByNumeroExposicion(numeroExposicion);
        if (!exposicion.isPresent()) {
            throw new Exception("la exposicion con número" + numeroExposicion + "no existe");
        }
        return exposicion.get();
    }

    @PostMapping("/exposicion")
    public Exposicion addCoche(@RequestBody Exposicion exposicion) throws Exception {
        Optional<Exposicion> lista = exposicionRepository.findByNumeroExposicion(exposicion.getNumeroExposicion());
        if (lista.isPresent())
            throw new Exception("La exposición indicada ya existe"
            );

        exposicionRepository.save(exposicion);
        return exposicion;
    }

    @GetMapping("/exposiciones")
    public List exposiciones() throws Exception {
        List<Exposicion> exposicions = exposicionRepository.findAll();
        if (exposicions.isEmpty()) {
            throw new Exception("no hay coches");
        } else {
            return exposicions;
        }
    }

    @GetMapping("/exposiciones/{numeroExposicion}")
    public List CochesenExposicion(@PathVariable int numeroExposicion) throws Exception {

        Optional<Exposicion> exposicion = exposicionRepository.findByNumeroExposicion(numeroExposicion);
        if (!exposicion.isPresent()) {
            throw new Exception("la exposicion con número" + numeroExposicion + "no existe");
        }
        List<Coche> coches = cocheRepository.findByNumeroExposicion(numeroExposicion);
        if (coches.isEmpty()) {
            throw new Exception("esta exposición no tiene coches");
        }
        return coches;
    }


    @DeleteMapping("/BorrarExposicion/{numeroExposicion}")//no guarda en elrepositoryde cochessinexponer

    public void delecteExpo(@PathVariable Integer numeroExposicion) throws Exception {
        List<Coche> coches = cocheRepository.findByNumeroExposicion(numeroExposicion);
        if (!coches.isEmpty()) {
            throw new Exception("no puedes borrar una exposición con coches dentro.Cambia los coches de exposición");
        }
        Optional<Exposicion> expo = exposicionRepository.findByNumeroExposicion(numeroExposicion);
        if (expo.isPresent()) {
            exposicionRepository.deleteByNumeroExposicion(numeroExposicion);
        } else {
            throw new Exception("la exposición" + numeroExposicion + " no existe");
        }

    }

    @PutMapping("/updateExpo")
    public Exposicion updateIt(@RequestBody Exposicion exposicion) throws Exception {

        Optional<Exposicion> lista2 = exposicionRepository.findByNumeroExposicion(exposicion.getNumeroExposicion());
        if (lista2.isPresent()) {
            throw new Exception("la exposición indicada ya está registrada");
        }
        exposicionRepository.save(exposicion);

        return exposicion;
    }

}






package com.curso.demo.controllers;

import com.curso.demo.model.Coche;
import com.curso.demo.model.EstadoCoche;
import com.curso.demo.model.Exposicion;
import com.curso.demo.model.Reparacion;
import com.curso.demo.repositories.CocheEnReparacionRepository;
import com.curso.demo.repositories.CocheRepository;
import com.curso.demo.repositories.ExposicionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;



@RestController

public class ControllerCoche {
    @Autowired
    private CocheRepository cocheRepository;
    @Autowired
    private ExposicionRepository exposicionRepository;
    @Autowired
    private CocheEnReparacionRepository cocheEnReparacionRepository;


    @GetMapping("coche/{cocheId}")//funciona
    public Coche buscarCoche(@PathVariable Integer cocheId) throws Exception {
        Optional<Coche> coches = cocheRepository.findById(cocheId);
        if (!coches.isPresent()) {
            throw new Exception("el coche con id" + cocheId + "no existe");
        }
        return coches.get();
    }


    @GetMapping("/coches")//funciona
    public List coches() throws Exception {
        List<Coche> coches = cocheRepository.findAll();
        if (coches.isEmpty()) {
            throw new Exception("no hay coches");
        } else {
            return coches;
        }
    }

    @PostMapping("/AltaCoche")
    public Coche addCoche( @RequestBody Coche coche) throws Exception {

        Optional<Exposicion> lista2 = exposicionRepository.findByNumeroExposicion(coche.getNumeroExposicion());
        if (!lista2.isPresent()) {
            throw new Exception("la exposición indicada no existe");
        }
        cocheRepository.save(coche);
        return coche;
    }


    @DeleteMapping("/coche/{cocheId}")
    public void delecteCoche(@PathVariable Integer cocheId) throws Exception {
        Optional<Coche> coche = cocheRepository.findById(cocheId);
        if (coche.isPresent()) {
            cocheRepository.deleteById(cocheId);
        } else {
            throw new Exception("el coche con id" + cocheId + " no existe");
        }

    }


    @PutMapping("/update")
    public Coche updateIt(@RequestBody Coche coche) throws Exception {

        Optional<Coche> lista = cocheRepository.findByMatricula(coche.getMatricula());
        if (lista.isPresent()) {
            throw new Exception("ya existe un coche con esa matrícula");
        }
        Optional<Exposicion> lista2 = exposicionRepository.findByNumeroExposicion(coche.getNumeroExposicion());
        if (!lista2.isPresent()) {
            throw new Exception("la exposición indicada no existe");
        }

        if (coche.getEstadoCoche() == EstadoCoche.REPARACION) {
            if (coche.getTipoReparacion() == null) {
                throw new Exception("introduce el tipo de reparacion");
            }
            Reparacion reparacion = new Reparacion();
            reparacion.setTipoReparacion(coche.getTipoReparacion());
            reparacion.setReparacionId(coche.getCocheId());
            cocheEnReparacionRepository.save(reparacion);
        }
        cocheRepository.save(coche);


        return coche;
    }


    @GetMapping("/cochesReparacion")
    public List cochesEnReparacion() throws Exception {
        List<Coche> coches = cocheRepository.findByEstadoCoche(EstadoCoche.REPARACION);
        if (coches.isEmpty()) {
            throw new Exception("no hay coches en reparación");
        } else {
            return coches;
        }
    }

}







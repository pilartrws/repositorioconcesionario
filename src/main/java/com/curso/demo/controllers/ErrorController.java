package com.curso.demo.controllers;

import com.curso.demo.model.ErroresHandler;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.List;


@ControllerAdvice
public class ErrorController {

        @ExceptionHandler(MethodArgumentNotValidException.class)
        public ResponseEntity<ErroresHandler> methodArgumentNotValidException(HttpServletRequest request, MethodArgumentNotValidException e) {


            BindingResult result = e.getBindingResult();
            List<FieldError> fieldErrors = result.getFieldErrors();


            StringBuilder errorMessage = new StringBuilder();
            fieldErrors.forEach(errorString -> errorMessage.append(errorString.getField() + " " + errorString.getDefaultMessage() +  " "));


            ErroresHandler errorInfo = new ErroresHandler(HttpStatus.BAD_REQUEST.value(), errorMessage.toString(), request.getRequestURI());
            return new ResponseEntity<>(errorInfo, HttpStatus.BAD_REQUEST);

        }




}

